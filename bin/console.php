#!/usr/bin/env php7.4
<?php

declare(strict_types=1);

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;

(static function () {

    require 'bootstrap.php';

    /** @var \Interop\Container\ContainerInterface $container */
    $container = require 'config/container.php';

    $config = $container->get('config');

    /** @var Application $app */
    $application = new Application();

    foreach ($config['commands'] as $command) {
        if (!is_subclass_of($command, Command::class)) {
            continue;
        }

        $application->add($container->get($command));
    }

    $application->run();
})();
