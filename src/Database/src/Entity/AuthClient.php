<?php

declare(strict_types=1);

namespace Database\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Database\Repository\AuthClientRepository")
 * @ORM\Table(name="oauth_clients")
 */
class AuthClient
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="client_id", type="string", length=80, nullable=false)
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="client_secret", type="string", length=80, nullable=false)
     */
    protected $secret;

    /**
     * @var string|null
     * @ORM\Column(name="scope", type="string", length=4000, nullable=true)
     */
    protected $scope;

    /**
     * @var string
     * @ORM\Column(name="private_key", type="text", nullable=false)
     */
    protected $privateKey;

    /**
     * @var string
     * @ORM\Column(name="public_key", type="text", nullable=false)
     */
    protected $publicKey;


    public function getId(): string
    {
        return $this->id;
    }


    public function setId(string $id): AuthClient
    {
        $this->id = $id;
        return $this;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function setSecret(string $secret): AuthClient
    {
        $this->secret = $secret;
        return $this;
    }

    public function getScope(): ?string
    {
        return $this->scope;
    }

    public function setScope(?string $scope): AuthClient
    {
        $this->scope = $scope;
        return $this;
    }

    public function getPrivateKey(): string
    {
        return $this->privateKey;
    }

    public function setPrivateKey(string $privateKey): AuthClient
    {
        $this->privateKey = $privateKey;
        return $this;
    }

    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    public function setPublicKey(string $publicKey): AuthClient
    {
        $this->publicKey = $publicKey;
        return $this;
    }

    public function verifyClientSecret(string $clientSecret): bool
    {
        return $this->getSecret() === $clientSecret;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        return [
            'client_id' => $this->id,
            'client_secret' => $this->secret,
            'scope' => $this->scope,
            'public_key' => $this->publicKey,
            'private_key' => $this->privateKey,
        ];
    }
}
