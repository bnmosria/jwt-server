<?php

declare(strict_types=1);

namespace Database\Repository;

use Database\Entity\AuthClient;
use Doctrine\ORM\EntityRepository;

class AuthClientRepository extends EntityRepository
{
    public function find($id, $lockMode = null, $lockVersion = null): ?AuthClient
    {
        /** @var AuthClient|null $client */
        return parent::find($id);
    }
}
