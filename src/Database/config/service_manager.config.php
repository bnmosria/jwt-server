<?php

return [
    'aliases' => [
        \Doctrine\ORM\EntityManagerInterface::class => \Doctrine\ORM\EntityManager::class,
    ],
    'invokables' => [
        \Database\Entity\AuthClient::class => \Database\Entity\AuthClient::class,
    ],
];
