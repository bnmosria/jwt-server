<?php

return [
    'factories'  => [
        \Command\AuthClient\CreateAuthClientCommand::class => \Command\AuthClient\Factory\CreateAuthClientCommandFactory::class
    ],
];
