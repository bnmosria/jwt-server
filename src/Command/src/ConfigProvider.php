<?php

declare(strict_types=1);

namespace Command;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'commands' => $this->getCommandsConfig(),
        ];
    }

    public function getDependencies(): array
    {
        return include __DIR__ . '/../config/service_manager.config.php';
    }

    public function getCommandsConfig(): array
    {
        return include __DIR__ . '/../config/commands.config.php';
    }
}
