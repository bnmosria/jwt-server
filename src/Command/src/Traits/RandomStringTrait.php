<?php

declare(strict_types=1);

namespace Command\Traits;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Command\src\Traits
 */
trait RandomStringTrait
{
    public function create(
        int $length = 32,
        string $keySpace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): string {

        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }

        $pieces = [];

        $max = mb_strlen($keySpace, '8bit') - 1;

        for ($i = 0; $i < $length; ++$i) {
            try {
                $pieces [] = $keySpace[random_int(0, $max)];
            }
            catch (\Exception $e) {}
        }

        return implode('', $pieces);
    }
}
