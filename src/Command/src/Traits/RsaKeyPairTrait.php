<?php

declare(strict_types=1);

namespace Command\Traits;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Command\src\Traits
 */
trait RsaKeyPairTrait
{
    function generate(array $config = []): array
    {
        $config = array_merge([
              'digest_alg' => 'sha512',
              'private_key_bits' => 2048,
              'private_key_type' => OPENSSL_KEYTYPE_RSA,
        ], $config);

        $key = openssl_pkey_new($config);
        if (!$key) {
            throw new \RuntimeException('Unable to generate RSA key pair');
        }

        openssl_pkey_export($key, $private);
        if (!$private) {
            throw new \RuntimeException('Unable to extract private key');
        }

        $public = openssl_pkey_get_details($key)['key'] ?? null;
        if (!$public) {
            throw new \RuntimeException('Unable to extract public key');
        }

        openssl_pkey_free($key);

        return compact('public', 'private');
    }
}
