<?php

namespace Command\AuthClient\Factory;

use Command\AuthClient\CreateAuthClientCommand;
use Interop\Container\ContainerInterface;

/**
 * @author Benjamin Osoria Peralta <bnmosria@hotmail.com>
 */
class CreateAuthClientCommandFactory
{
    public function __invoke(ContainerInterface $container): CreateAuthClientCommand
    {
        return new CreateAuthClientCommand($container);
    }
}
