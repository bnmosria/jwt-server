<?php

declare(strict_types=1);

namespace Command\AuthClient;

use Command\Traits\RandomStringTrait;
use Command\Traits\RsaKeyPairTrait;
use Database\Entity\AuthClient;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Command\src\AuthClient
 */
class CreateAuthClientCommand extends Command
{
    use RandomStringTrait;
    use RsaKeyPairTrait;

    protected static $defaultName = 'auth:client:create';

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Create an auth client')
            ->setHelp(
                <<<EOT
The <info>%command.name%</info> command, will creates an auth client.
A client id, a client secret, a private and a public key  will be generated to be used as credentials to get a json web token from the auth server.
The public key will be also need it in the resource server to validate the json web token.
EOT
            );
    }

    public function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $authClient = new AuthClient();

        $pemsKeyPair = $this->generate();
        $entityManager = $this->container->get(EntityManager::class);

        $authClient
            ->setId($this->create())
            ->setSecret($this->create())
            ->setPrivateKey($pemsKeyPair['private'])
            ->setPublicKey($pemsKeyPair['public']);

        $entityManager->persist($authClient);
        $entityManager->flush();

        $symfonyStyle = new SymfonyStyle($input, $output);
        $symfonyStyle->success('New auth client created!');

        $symfonyStyle->title('Created credentials: ');
        $symfonyStyle->writeln('<comment>Client ID:</comment>');
        $symfonyStyle->text($authClient->getId());
        $symfonyStyle->newLine();
        $symfonyStyle->writeln('<comment>Client Secret:</comment>');
        $symfonyStyle->text($authClient->getSecret());
        $symfonyStyle->newLine();
        $symfonyStyle->writeln('<comment>Public Key:</comment>');
        $symfonyStyle->text($authClient->getPublicKey());

        return 0;
    }
}
