<?php

namespace Infrastructure\RouteStackFactory;

use Psr\Container\ContainerInterface;
use Mezzio\Router\LaminasRouter as Zf2Bridge;
use Laminas\Router\Http\TreeRouteStack;

class RouterFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return Zf2Bridge
     */
    public function __invoke(ContainerInterface $container)
    {
        return new Zf2Bridge($container->get(TreeRouteStack::class));
    }
}
