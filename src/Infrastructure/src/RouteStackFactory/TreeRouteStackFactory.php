<?php

namespace Infrastructure\RouteStackFactory;

use Psr\Container\ContainerInterface;
use Laminas\Router\Http\TreeRouteStack;

class TreeRouteStackFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return TreeRouteStack
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = new TreeRouteStack();

        return $router;
    }
}
