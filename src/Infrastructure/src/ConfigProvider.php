<?php

declare(strict_types=1);

namespace Infrastructure;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    public function getDependencies(): array
    {
        return include __DIR__ . '/' . '../config/provider.config.php';
    }
}
