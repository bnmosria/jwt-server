<?php


use Infrastructure\RouteStackFactory\RouterFactory;
use Infrastructure\RouteStackFactory\TreeRouteStackFactory;
use Laminas\Router\Http\TreeRouteStack;
use Mezzio\Router\RouterInterface;

return [
    'factories'  => [
        TreeRouteStack::class  => TreeRouteStackFactory::class,
        RouterInterface::class => RouterFactory::class,
    ],
];
