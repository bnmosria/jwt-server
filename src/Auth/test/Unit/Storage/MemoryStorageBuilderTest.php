<?php

namespace Auth\Unit\Storage;


use Auth\Storage\MemoryStorageBuilder;
use Database\Entity\AuthClient;
use Filesystem\Filesystem\LocalFilesystem;
use Laminas\Config\Config;
use OAuth2\Storage\Memory;
use PHPUnit\Framework\TestCase;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Unit\Storage
 */
class MemoryStorageBuilderTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnAMemoryStorage(): void
    {
        $client = $this->prophesize(AuthClient::class);

        $client->getId()->willReturn('foo');
        $client->getSecret()->willReturn('baz');
        $client->getPublicKey()->willReturn('baz');
        $client->getPrivateKey()->willReturn('baz');

        $config = $this->prophesize(Config::class);

        $builder = new MemoryStorageBuilder($config->reveal());

        $storage = $builder->build($client->reveal());

        $this->assertInstanceOf(Memory::class, $storage);
    }
}
