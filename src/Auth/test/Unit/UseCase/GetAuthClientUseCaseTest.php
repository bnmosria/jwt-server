<?php

namespace Auth\Unit\UseCase;


use Auth\Exception\AuthClientForbiddenException;
use Auth\Exception\AuthClientNotFoundException;
use Auth\UseCase\GetAuthClientUseCase;
use Database\Entity\AuthClient;
use Database\Repository\AuthClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Unit\UseCase
 */
class GetAuthClientUseCaseTest extends TestCase
{
    /**
     * @test
     */
    public function executeShouldReturnAClient(): void
    {
        $repository = $this->prophesize(AuthClientRepository::class);
        $repository->find(Argument::any())
            ->willReturn($this->getMockClient());

        $entityManager = $this->prophesize(EntityManagerInterface::class);
        $entityManager
            ->getRepository(Argument::any())
            ->willReturn($repository->reveal());

        $useCase = new GetAuthClientUseCase($entityManager->reveal());

        $client = $useCase->execute(
            [
                'client_id' => 'foo',
                'client_secret' => 'baz'
            ]
        );

        $this->assertInstanceOf(AuthClient::class, $client);
        $this->assertSame('foo', $client->getId());
    }

    /**
     * @test
     */
    public function executeShouldReturnANotFoundException(): void
    {
        $repository = $this->prophesize(AuthClientRepository::class);
        $repository->find(Argument::any())
            ->willReturn(null);

        $entityManager = $this->prophesize(EntityManagerInterface::class);
        $entityManager
            ->getRepository(Argument::any())
            ->willReturn($repository->reveal());

        $useCase = new GetAuthClientUseCase($entityManager->reveal());

        $this->expectException(AuthClientNotFoundException::class);

        $useCase->execute(
            [
                'client_id' => 'foo',
                'client_secret' => 'baz'
            ]
        );
    }

    /**
     * @test
     */
    public function executeShouldReturnAForbiddenException(): void
    {
        $repository = $this->prophesize(AuthClientRepository::class);
        $repository->find(Argument::any())
            ->willReturn($this->getMockClient());

        $entityManager = $this->prophesize(EntityManagerInterface::class);
        $entityManager
            ->getRepository(Argument::any())
            ->willReturn($repository->reveal());

        $useCase = new GetAuthClientUseCase($entityManager->reveal());

        $this->expectException(AuthClientForbiddenException::class);

        $useCase->execute(
            [
                'client_id' => 'foo',
                'client_secret' => 'bazz'
            ]
        );
    }

    private function getMockClient(): AuthClient
    {
        return (new AuthClient())
            ->setId('foo')
            ->setSecret('baz');
    }
}
