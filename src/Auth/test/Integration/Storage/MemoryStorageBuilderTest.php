<?php

namespace Auth\Integration\Storage;

use Auth\Exception\MissingEncryptionKeysException;
use Auth\Storage\MemoryStorageBuilder;
use Database\Entity\AuthClient;
use Filesystem\Filesystem\LocalFilesystem;
use Laminas\Config\Config;
use League\Flysystem\Adapter\Local;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Unit\Storage
 */
class MemoryStorageBuilderTest extends TestCase
{
    /**
     * @test
     */
    public function returnedStorageShouldContainTheCorrectClientValues(): void
    {
        $client = $this->getClient();

        $builder = new MemoryStorageBuilder($this->getConfig());

        $storage = $builder->build($client);

        $details = $storage->getClientDetails('foo');

        $this->assertSame($details['client_id'], $client->getId());
        $this->assertSame($details['client_secret'], $client->getSecret());
    }

    /**
     * @test
     */
    public function returnedStorageShouldContainTheCorrectFilesystemContent(): void
    {
        $client = $this->getClient();

        $builder = new MemoryStorageBuilder($this->getConfig());

        $storage = $builder->build($client);

        $this->assertSame($storage->keys['public_key'], 'abcde');
        $this->assertSame($storage->keys['private_key'], 'fghij');
    }


    private function getClient(): AuthClient
    {
        return (new AuthClient())
            ->setId('foo')
            ->setSecret('baz')
            ->setPublicKey('abcde')
            ->setPrivateKey('fghij');
    }

    private function getConfig(): Config
    {
        return new Config([]);
    }
}
