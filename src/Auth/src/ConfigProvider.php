<?php

declare(strict_types=1);

namespace Auth;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'routes'       => $this->getRoutes(),
            'auth' => $this->getConfig(),
        ];
    }

    public function getDependencies(): array
    {
        return include __DIR__ . '/../config/service_manager.config.php';
    }

    public function getRoutes(): array
    {
        return include __DIR__ . '/../config/routes.config.php';
    }

    public function getConfig(): array
    {
        return include __DIR__ . '/../config/auth.config.php';
    }
}
