<?php

declare(strict_types=1);

namespace Auth\Exception;

use Laminas\Http\Exception\RuntimeException;
use Laminas\Http\Response;
use Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Exception
 */
class MissingEncryptionKeysException extends RuntimeException implements AuthExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    private const EXCEPTION_TITLE = 'Missing encryption keys';

    private const STATUS = Response::STATUS_CODE_400;
    private const EXCEPTION_MESSAGE_WRONG_CONFIGURATION = 'The private and/or public key are missing';
    private const TYPE = 'https://example.com/problems/missing-encription-keys';

    public static function fromClientEntity(array $additional = null): MissingEncryptionKeysException
    {
        $detail = self::EXCEPTION_MESSAGE_WRONG_CONFIGURATION;

        $exception = new self($detail);

        $exception->status = self::STATUS;
        $exception->detail = $detail;
        $exception->title = self::EXCEPTION_TITLE;
        $exception->type = self::TYPE;
        $exception->additional = $additional ?? [];

        return $exception;
    }
}
