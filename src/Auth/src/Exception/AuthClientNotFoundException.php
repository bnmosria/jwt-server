<?php

declare(strict_types=1);

namespace Auth\Exception;

use Laminas\Http\Exception\RuntimeException;
use Laminas\Http\Response;
use Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Exception
 */
class AuthClientNotFoundException extends RuntimeException implements AuthExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    private const EXCEPTION_TITLE = 'Not found';

    private const STATUS_CLIENT_ID_NOT_FOUND = Response::STATUS_CODE_404;
    private const EXCEPTION_MESSAGE_CLIENT_ID_NOT_FOUND = 'An oauth client entity with client id "%s" was not found';
    private const TYPE = 'https://example.com/problems/oauth-client-not-found';

    public static function fromClientId(string $clientId, array $additional = null): AuthClientNotFoundException
    {
        $detail = sprintf(self::EXCEPTION_MESSAGE_CLIENT_ID_NOT_FOUND, $clientId);
        
        $exception = new self($detail);

        $exception->status = self::STATUS_CLIENT_ID_NOT_FOUND;
        $exception->detail = $detail;
        $exception->title = self::EXCEPTION_TITLE;
        $exception->type = self::TYPE;
        $exception->additional = $additional ?? [];

        return $exception;
    }

}
