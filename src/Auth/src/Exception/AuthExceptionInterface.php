<?php

declare(strict_types=1);

namespace Auth\Exception;

use Mezzio\Exception\ExceptionInterface;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;
use Throwable;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Exception
 */
interface AuthExceptionInterface extends Throwable, ProblemDetailsExceptionInterface
{
}
