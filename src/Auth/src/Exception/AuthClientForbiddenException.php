<?php

declare(strict_types=1);

namespace Auth\Exception;

use Laminas\Http\Exception\RuntimeException;
use Laminas\Http\Response;
use Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Exception
 */
class AuthClientForbiddenException extends RuntimeException implements AuthExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    private const EXCEPTION_TITLE = 'Forbidden';

    private const STATUS_WRONG_CREDENTIALS = Response::STATUS_CODE_403;
    private const EXCEPTION_MESSAGE_WRONG_CREDENTIALS = 'Wrong client id or client secret';

    private const TYPE = 'https://example.com/problems/oauth-client-forbidden';

    public static function fromWrongCredentials(): AuthClientForbiddenException
    {
        $exception = new self(self::EXCEPTION_MESSAGE_WRONG_CREDENTIALS);

        $exception->status = self::STATUS_WRONG_CREDENTIALS;
        $exception->detail = self::EXCEPTION_MESSAGE_WRONG_CREDENTIALS;
        $exception->title = self::EXCEPTION_TITLE;
        $exception->type = self::TYPE;
        $exception->additional = $additional ?? [];

        return $exception;
    }
}
