<?php

declare(strict_types=1);

namespace Auth\Exception;

use Laminas\Http\Exception\RuntimeException;
use Laminas\Http\Response;
use Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Exception
 */
class InvalidParametersException extends RuntimeException implements AuthExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    private const EXCEPTION_TITLE = 'Invalid credential parameters';
    private const EXCEPTION_MESSAGE = 'The given parameters are wrong';
    private const STATUS = Response::STATUS_CODE_400;
    private const TYPE = 'https://example.com/problems/invalid-parameters';

    public static function fromMessages(array $messages): InvalidParametersException
    {
        $exception = new self();

        $exception->status = self::STATUS;
        $exception->detail = self::EXCEPTION_MESSAGE;
        $exception->title = self::EXCEPTION_TITLE;
        $exception->type = self::TYPE;
        $exception->additional = $messages ?? [];

        return $exception;
    }
}
