<?php

declare(strict_types=1);

namespace Auth\Storage;

use Auth\Exception\MissingEncryptionKeysException;
use Database\Entity\AuthClient;
use Laminas\Config\Config;
use OAuth2\Storage\Memory;
use phpDocumentor\Reflection\Types\Nullable;
use Reinfi\DependencyInjection\Annotation\InjectConfig;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Storage
 */
class MemoryStorageBuilder
{
    /** @var string */
    private const OPTIONS_KEYS = 'keys';

    /** @var string */
    private const PUBLIC_KEY = 'public_key';

    /** @var string */
    private const PRIVATE_KEY = 'private_key';

    /** @var string */
    private const ENCRYPTION_ALG = 'encryption_algorithm';

    /** @var string */
    private const CLIENT_CREDENTIALS = 'client_credentials';

    /** @var string */
    private const CLIENT_SECRET = 'client_secret';

    /** @var Config */
    private $config;

    /**
     * @param Config $config
     *
     * @InjectConfig("auth")
     */
    public function __construct(Config $config) {
        $this->config = $config;
    }

    public function build(AuthClient $client): Memory
    {
        if ($client->getPublicKey() === null || $client->getPrivateKey() === null) {
            throw MissingEncryptionKeysException::fromClientEntity();
        }

        return new Memory($this->buildParameters($client));
    }

    /**
     * @param AuthClient $client
     *
     * @return string[]
     */
    private function buildParameters(AuthClient $client): array
    {
        return [
            self::OPTIONS_KEYS       => [
                self::PUBLIC_KEY     => $client->getPublicKey(),
                self::PRIVATE_KEY    => $client->getPrivateKey(),
                self::ENCRYPTION_ALG => $this->config->encryption_alg
            ],
            self::CLIENT_CREDENTIALS => [
                $client->getId() => [self::CLIENT_SECRET => $client->getSecret()]
            ],
        ];
    }
}
