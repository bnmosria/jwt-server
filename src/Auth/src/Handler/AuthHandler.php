<?php

declare(strict_types=1);

namespace Auth\Handler;

use Auth\Services\AuthService;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AuthHandler implements RequestHandlerInterface
{
    /** @var AuthService */
    private $authService;

    public function __construct(AuthService $authService) {
        $this->authService = $authService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->authService->handleToken($request->getParsedBody());

        return new JsonResponse(
            $this->authService->getResponse()
                ->getParameters(),
            $this->authService->getResponse()
                ->getStatusCode()
        );
    }
}
