<?php

declare(strict_types=1);

namespace Auth\UseCase;

use Auth\Exception\AuthClientForbiddenException;
use Auth\Exception\AuthClientNotFoundException;
use Database\Entity\AuthClient;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\UseCase
 */
class GetAuthClientUseCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function execute(array $credentials): AuthClient
    {
        $clientRepository = $this->entityManager
            ->getRepository(AuthClient::class);

        /** @var AuthClient $client */
        $client = $clientRepository->find($credentials['client_id']);

        if ($client === null) {
            throw AuthClientNotFoundException::fromClientId($credentials['client_id']);
        }

        if (!$client->verifyClientSecret($credentials['client_secret'])) {
            throw AuthClientForbiddenException::fromWrongCredentials();
        }

        return $client;
    }
}
