<?php

declare(strict_types=1);

namespace Auth\Middleware;

use Auth\Exception\InvalidParametersException;
use Auth\InputFilter\ClientCredentialsInputFilter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\InputFilter
 */
class ClientCredentialsInputFilterMiddleware implements MiddlewareInterface
{
    /** @var ClientCredentialsInputFilter */
    private $clientCredentialsInputFilter;

    public function __construct(ClientCredentialsInputFilter $clientCredentialsInputFilter)
    {
        $this->clientCredentialsInputFilter = $clientCredentialsInputFilter;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $this->clientCredentialsInputFilter->setData($request->getParsedBody());

        if (!$this->clientCredentialsInputFilter->isValid()) {
            throw InvalidParametersException::fromMessages($this->clientCredentialsInputFilter->getMessages());
        }

        return $handler->handle($request);
    }
}
