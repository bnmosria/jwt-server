<?php

namespace Auth\Services;

use Auth\UseCase\GetAuthClientUseCase;
use Laminas\Config\Config;
use OAuth2\Request;
use OAuth2\Server;
use OAuth2\Storage\ClientCredentialsInterface;
use Reinfi\DependencyInjection\Annotation\Inject;
use Reinfi\DependencyInjection\Annotation\InjectConfig;
use Auth\Storage\MemoryStorageBuilder;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\Services
 */
class AuthService extends Server
{
    /** @var MemoryStorageBuilder */
    private $storageBuilder;

    /** @var \Auth\UseCase\GetAuthClientUseCase */
    private $getOauthClientUseCase;

    /**
     * AuthService constructor.
     *
     * @param MemoryStorageBuilder $storageBuilder
     * @param \Auth\UseCase\GetAuthClientUseCase         $getOauthClientUseCase
     * @param \Laminas\Config\Config                     $config
     *
     * @Inject(MemoryStorageBuilder::class)
     * @Inject(GetAuthClientUseCase::class)
     * @InjectConfig("auth.server")
     */
    public function __construct(
        MemoryStorageBuilder $storageBuilder,
        GetAuthClientUseCase $getOauthClientUseCase,
        Config $config
    ) {
        parent::__construct([], $config->toArray());

        $this->storageBuilder = $storageBuilder;
        $this->getOauthClientUseCase = $getOauthClientUseCase;
    }

    public function handleToken(array $credentials): void
    {
        $client = $this->getOauthClientUseCase->execute($credentials);

        $this->addStorage($this->storageBuilder->build($client));

        $this->handleTokenRequest(Request::createFromGlobals());
    }
}
