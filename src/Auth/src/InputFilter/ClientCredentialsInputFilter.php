<?php

declare(strict_types=1);

namespace Auth\InputFilter;

use Laminas\InputFilter\Input;
use Laminas\InputFilter\InputFilter;
use Laminas\Validator\NotEmpty;

/**
 * @author  Benjamin Osoria Peralta <bnmosria@hotmail.com>
 * @package Auth\InputFilter
 */
class ClientCredentialsInputFilter extends InputFilter
{
    public const PARAMETER_CLIENT_ID = 'client_id';
    public const PARAMETER_CLIENT_SECRET = 'client_secret';
    public const PARAMETER_GRANT_TYPE = 'grant_type';

    public function __construct()
    {
        $clientIdInput = new Input(self::PARAMETER_CLIENT_ID);
        $clientSecretInput = new Input(self::PARAMETER_CLIENT_SECRET);
        $grantTypeInput = new Input(self::PARAMETER_GRANT_TYPE);

        $clientIdInput
            ->setRequired(true)
            ->getValidatorChain()
            ->attach(new NotEmpty());

        $clientSecretInput
            ->setRequired(true)
            ->getValidatorChain()
            ->attach(new NotEmpty());

        $grantTypeInput
            ->setRequired(true)
            ->getValidatorChain()
            ->attach(new NotEmpty());

        $this
            ->add($clientIdInput)
            ->add($clientSecretInput)
            ->add($grantTypeInput);
    }
}
