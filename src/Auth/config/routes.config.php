<?php

use Auth\Handler\AuthHandler;
use Auth\Middleware\ClientCredentialsInputFilterMiddleware;

return [
    [
        'path'            => '/auth',
        'middleware'      => [
            ClientCredentialsInputFilterMiddleware::class,
            AuthHandler::class,

        ],
        'allowed_methods' => ['POST'],
    ],
];
