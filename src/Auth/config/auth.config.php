<?php

declare(strict_types=1);

return [
    'server' => [
        'use_jwt_access_tokens' => true,
    ],
    'encryption_alg' => 'RS256',
];
