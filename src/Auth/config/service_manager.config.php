<?php

use Auth\Handler\AuthHandler;
use Reinfi\DependencyInjection\Factory\AutoWiringFactory;

return [
    'factories' => [
        AuthHandler::class                        => AutoWiringFactory::class,
        \Auth\Services\AuthService::class         => \Reinfi\DependencyInjection\Factory\InjectionFactory::class,
        \Auth\Storage\MemoryStorageBuilder::class => \Reinfi\DependencyInjection\Factory\InjectionFactory::class,

        \Auth\UseCase\GetAuthClientUseCase::class => AutoWiringFactory::class,

        \Auth\InputFilter\ClientCredentialsInputFilter::class          => AutoWiringFactory::class,
        \Auth\Middleware\ClientCredentialsInputFilterMiddleware::class => AutoWiringFactory::class,
    ],
];
