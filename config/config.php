<?php

declare(strict_types=1);

use Laminas\ConfigAggregator\ArrayProvider;
use Laminas\ConfigAggregator\ConfigAggregator;
use Laminas\ConfigAggregator\PhpFileProvider;

$cacheConfig = [
    'config_cache_path' => 'data/cache/config-cache.php',
];

\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(
    function ($className) {
        return class_exists($className);
    }
);

$aggregator = new ConfigAggregator(
    [
        \Mezzio\Helper\ConfigProvider::class,
        \Mezzio\ProblemDetails\ConfigProvider::class,
        \DoctrineORMModule\ConfigProvider::class,
        \DoctrineModule\ConfigProvider::class,
        \Laminas\Form\ConfigProvider::class,
        \Laminas\InputFilter\ConfigProvider::class,
        \Laminas\Filter\ConfigProvider::class,
        \Laminas\Paginator\ConfigProvider::class,
        \Laminas\Hydrator\ConfigProvider::class,
        \Reinfi\DependencyInjection\ConfigProvider::class,
        \Laminas\Cache\ConfigProvider::class,
        \Mezzio\Router\LaminasRouter\ConfigProvider::class,
        \Laminas\Router\ConfigProvider::class,
        \Laminas\Validator\ConfigProvider::class,
        \Laminas\HttpHandlerRunner\ConfigProvider::class,
        new ArrayProvider($cacheConfig),
        new \Laminas\ConfigAggregatorModuleManager\LaminasModuleProvider(
            new Reinfi\DependencyInjection\Module()
        ),
        new \Laminas\ConfigAggregatorModuleManager\LaminasModuleProvider(
            new \DoctrineModule\Module()
        ),
        new \Laminas\ConfigAggregatorModuleManager\LaminasModuleProvider(
            new \DoctrineORMModule\Module()
        ),
        \Mezzio\ConfigProvider::class,
        \Mezzio\Router\ConfigProvider::class,

        // Application modules
        \Infrastructure\ConfigProvider::class,
        \Database\ConfigProvider::class,
        \Auth\ConfigProvider::class,
        \Command\ConfigProvider::class,

        new PhpFileProvider(realpath(__DIR__) . '/autoload/{{,*.}global,{,*.}local}.php'),
        new PhpFileProvider(realpath(__DIR__) . '/development.config.php'),
    ],
    $cacheConfig['config_cache_path']
);

$mergedConfig = $aggregator->getMergedConfig();

return $mergedConfig;
